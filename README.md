# Configure the project

## Environment

- Edit .env like .env.example to set the configurations.

- Install the packages: `npm install`

- Up the database service: `docker-compose up` and create a database specified in .env

- Run the migrations: `npm run migration`
- Run the seeds: `npm run seed`

- Start the project in development `npm run dev` or
- Compile and run in production `npm start`
